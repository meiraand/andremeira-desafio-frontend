import React, { Component } from 'react';
import './styles/global';
import Head from './components/Head';
import Cards from './components/Cards';
import axios from 'axios';
import md5 from 'js-md5'

const PUBLIC_KEY = 'fa5845dbbb731a3565d1e8a6037fb56e'
const PRIVATE_KEY = 'f308ef5d1c3476e2e7bd418dec5c292fa2b2fa67'


export default class Main extends Component {

  constructor(props){
    
    super(props);
    this.state = {
      score : [],
    }

  }

  async componentDidMount() {
    // const response = await axios.get('https://sample-api-78c77.firebaseio.com/episodes/SHOW123.json');
    
    // //Retirando o valor null e montando um novo array.
    // const newArray = response.data.filter(e => e !== null);
    // this.setState({ episodes: newArray });

    // return axios.get('https://sample-api-78c77.firebaseio.com/tv-shows/SHOW123.json')
    // .then(response => {
    //   console.log(response);
    //   this.setState({ score: response.data });
    // });

        const timestamp = Number(new Date())
        const hash = md5.create()
        hash.update(timestamp + PRIVATE_KEY + PUBLIC_KEY)

        const response = await axios(`https://gateway.marvel.com:443/v1/public/characters?name=Iron%20Man&orderBy=modified&ts=${timestamp}&apikey=${PUBLIC_KEY}&hash=${hash.hex()}`)
        console.log(response);
        this.setState({ score: response.data });
  }

  render() {
    return (
      <div>
        <Head />
       
        { this.state.score.data && <Cards score={this.state.score}/>  }
        
      </div>
    );
  }
}
