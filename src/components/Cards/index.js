import React from 'react';

import { Card } from './style';

//import logo from '../Head/images/iron-man-logo.png';

const Cards = ({ score }) => (

  <Card>

    {
        score.data.results.map((cast, index) => 
        <div className="box" key={index}> 
          <h5>{cast.name}</h5>
          <p>{cast.description}</p>
          <img src={{uri: `${cast.thumbnail.path}.${cast.thumbnail.extension}` }}   alt=""/>
          
        </div>
        )
    }
          
    
  </Card>
);

export default Cards;