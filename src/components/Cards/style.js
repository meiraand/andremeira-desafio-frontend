import styled from 'styled-components';

export const Card = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-content: flex-start;
  justify-content: flex-start;

  .box{
    width: 255px;
    height: 250px;
    color: #000;
    margin: 5px;
    border: 1px solid red;
  }
`;
