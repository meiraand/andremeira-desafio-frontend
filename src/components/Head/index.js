import React from 'react';

import { Header } from './style';

import logo from '../Head/images/iron-man-logo.png';

const Head = () => (
  <Header>
    <div className="logo">
      <img src={logo} alt="Iron Man"/>
    </div>
  </Header>

);

export default Head;