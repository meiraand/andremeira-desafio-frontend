import styled from 'styled-components';

export const Header = styled.div`
  background-color: red;
  display: flex;
  align-items: center;
  justify-content: center;

  .logo{
    padding: 5px;
  }
`;
