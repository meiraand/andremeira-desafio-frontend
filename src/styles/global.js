import { injectGlobal } from 'styled-components';

injectGlobal`
  *{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
  }

  body{
    background-color:#F5F5F5;
    font-family: sans-serif;
  }

  hr{
    background-color: #EEE;
    height: 1px;
    border: none;
  }
`;
